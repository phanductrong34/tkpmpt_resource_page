# Umaster resource_page

## Phân công công việc
https://trello.com/b/Pa6aLkay/resource-page-lms

1. Phan Đức Trọng - 20187279 (Trưởng nhóm)
- Tạo cấu trúc của project
- Viết vuex, store bên front cho app
- Trang Admin/Dashboard
- Sidenav
- Global Component: Loading, Toast, Image, NoData
- Login,Logout, Welcome, NotFound
- Config firebase
-Page Admin/Teacher

2. Nguyễn Xuân Quốc Thái - 20187275
- Page User/DashBoard
- Page User/Classroom
- Page User/Laboratory

3. Trần Anh Dũng - 20187228
- Setup + config Docker
- Feat: StudentManage
- Page: Admin/Course

4. Lê Chiến Thắng - 20187276
- Page: Admin/Resource
- Page: ResourceUser


## Project setup (Development)
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

